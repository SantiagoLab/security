using Aforo255.Cross.Discovery.Consul;
using Aforo255.Cross.Discovery.Fabio;
using Aforo255.Cross.Log.Src.Elastic;
using Aforo255.Cross.Metric.Metrics;
using Aforo255.Cross.Metric.Registry;
using Aforo255.Cross.Token.Src;
using Aforo255.Cross.Tracing.Src.Zipkin;
using Microsoft.EntityFrameworkCore;
using MS.AFORO255.Security.Data;
using MS.AFORO255.Security.Persistences;
using MS.AFORO255.Security.Services;
using Serilog;

var builder = WebApplication.CreateBuilder(args);

//CARGAR NACOS, Extrae las configuraciones
builder.Host.ConfigureAppConfiguration((host, builder) =>
{
    var c = builder.Build();
    builder.AddNacosConfiguration(c.GetSection("nacosConfig"));
});
//Metricas con Prometheus
builder.WebHost.UseAppMetrics();
//Sirve solo para ver las configuraciones
var config = builder.Configuration;

//Logs con Elastic y Kortana UI
ExtensionsElastic.ConfigureLog(builder.Configuration);
builder.WebHost.UseSerilog();
// Add services to the container.
builder.Services.AddControllers();

builder.Services.AddDbContext<ContextDatabase>(
    opt =>
    {
        //Si se usa NACOS usar "cn:mysql" si se usa del appsetting usar "mysql:cn"
        opt.UseMySQL(builder.Configuration["cn:mysql"]);
    });
builder.Services.AddScoped<IAccessService, AccessService>();
builder.Services.Configure<JwtOptions>(builder.Configuration.GetSection("jwt"));
//Descubrimiento de registro (Verifica el servicio si esta activo con un metodo simple)
builder.Services.AddConsul();
//Trazas distribuidas
builder.Services.AddJZipkin();

//Balanceador
builder.Services.AddFabio();
//Metricas con Prometheus
builder.Services.AddTransient<IMetricsRegistry, MetricsRegistry>();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.UseAuthorization();
app.MapControllers();
//Descubrimiento de registro (Verifica el servicio si esta activo con un metodo simple)
app.UseConsul();

DbCreated.CreateDbIfNotExists(app);
app.Run();