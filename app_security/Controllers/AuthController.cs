﻿using Aforo255.Cross.Metric.Registry;
using Aforo255.Cross.Token.Src;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using MS.AFORO255.Security.DTOs;
using MS.AFORO255.Security.Services;
using System.Text.Json;

namespace MS.AFORO255.Security.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AuthController : ControllerBase
{
    private readonly IAccessService _accessService;
    private readonly JwtOptions _jwtOption;
    private readonly IMetricsRegistry _metricsRegistry;
    private readonly ILogger<AuthController> _logger;

    public AuthController(IAccessService accessService, IOptionsSnapshot<JwtOptions> jwtOption
        , IMetricsRegistry metricsRegistry, ILogger<AuthController> logger)
    {
        _accessService = accessService;
        _jwtOption = jwtOption.Value;
        _metricsRegistry = metricsRegistry;
        _logger = logger;
    }

    [HttpGet]
    public IActionResult Get()
    {
        _metricsRegistry.IncrementFindQuery();
        return Ok(_accessService.GetAll());
    }

    [HttpPost]
    public IActionResult Post([FromBody] AuthRequest request)
    {
        _logger.LogInformation("Post in AuthController with {0}", JsonSerializer.Serialize(request));

        if (!_accessService.Validate(request.UserName, request.Password))
        {
            return Unauthorized();
        }

        string token = JwtToken.Create(_jwtOption);
        Response.Headers.Add("access-control-expose-headers", "Authorization");
        Response.Headers.Add("Authorization", token);
        return Ok(new AuthResponse(token, "5h"));
    }
}

