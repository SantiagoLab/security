﻿using Microsoft.AspNetCore.Mvc;

namespace MS.AFORO255.Security.Controllers
{
    [Route("")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        //Descubrimiento de registro (Verifica el servicio si esta activo con un metodo simple)
        [HttpGet("ping")]
        public IActionResult Ping()
        {
            _logger.LogDebug("Ping ...");
            return Ok();
        }
    }
}